//
//  CorgiSprite.h
//  CorgidactylAnniversary
//
//  Created by James Cash on 05-07-13.
//  Copyright (c) 2013 OccasionallyCogent. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface CorgiSpriteNode : SKSpriteNode

@property (nonatomic,getter=isJumping) BOOL jumping;

+ (void)loadGameAssets;
- (void)startRunning;
- (void)jumpBy:(float)jumpHeight withDuration:(NSTimeInterval)jumpDuration;
- (void)landed;
- (void)gotHit;

@end
