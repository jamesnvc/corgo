//
//  PteroSprite.h
//  CorgidactylAnniversary
//
//  Created by James Cash on 06-07-13.
//  Copyright (c) 2013 OccasionallyCogent. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface PteroSpriteNode : SKSpriteNode

+ (void)loadGameAssets;

// Pterodactyl swoops to the given location in the given time & executes `complete` when done.
// Return YES if swoop completed, NO otherwise (when another action is already executing).
- (BOOL)swoopTo:(CGPoint)location
   withDuration:(NSTimeInterval)swoopDuration
     completion:(void (^)(void))complete;
// Pterodactyl flys out of the scene.
// Return YES if swoop completed, NO otherwise (when another action is already executing).
- (BOOL)flyAway;

- (void)startFlapping;
- (void)stopFlapping;
- (void)abortActions;

@end
