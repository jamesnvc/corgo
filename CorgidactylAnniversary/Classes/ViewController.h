//
//  ViewController.h
//  CorgidactylAnniversary
//

//  Copyright (c) 2013 OccasionallyCogent. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet UIButton *startPlayingButton;
@property (weak, nonatomic) IBOutlet UIImageView *loadingScreenImage;

- (IBAction)startGame:(id)sender;

@end
