//
//  AppDelegate.h
//  CorgidactylAnniversary
//
//  Created by James Cash on 02-07-13.
//  Copyright (c) 2013 OccasionallyCogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
