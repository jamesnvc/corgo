//
//  MyScene.h
//  CorgidactylAnniversary
//

//  Copyright (c) 2013 OccasionallyCogent. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

static const uint32_t kCategoryCorgi = 0x1 << 0;
static const uint32_t kCategoryObstacle = 0x1 << 1;
static const uint32_t kCategoryGround = 0x1 << 2;
static const uint32_t kCategoryPtero = 0x1 << 3;

@interface CorgoGameScene : SKScene

+ (void)loadGameAssets;
+ (void)loadGameAssetsWithCompletion:(void (^)(void))complete;

@end
