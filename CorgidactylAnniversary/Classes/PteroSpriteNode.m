//
//  PteroSprite.m
//  CorgidactylAnniversary
//
//  Created by James Cash on 06-07-13.
//  Copyright (c) 2013 OccasionallyCogent. All rights reserved.
//

#import "PteroSpriteNode.h"
#import "CorgoGameScene.h"

@interface PteroSpriteNode ()
{
    BOOL _swooping;
    BOOL _leaving;
}
@end

@implementation PteroSpriteNode

#pragma mark - Lifecycle

- (instancetype)init
{
    if (self = [super initWithImageNamed:@"ptero-base"]) {
        [self startFlapping];

        // Create physics body
        self.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:self.size.width * 0.5];
        self.physicsBody.categoryBitMask = kCategoryPtero;
        self.physicsBody.collisionBitMask = 0;
        self.physicsBody.affectedByGravity = NO;
        self.physicsBody.mass = 1000;
    }
    return self;
}

#pragma mark - Loading shared assets

static SKEmitterNode *sharedSparkEffect = nil;
static SKAction *sharedFlappingAction = nil;
+ (void)loadGameAssets
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dispatch_semaphore_t textureSemaphore = dispatch_semaphore_create(0);
        // Create flying animation
        NSMutableArray *flapTextures = [NSMutableArray array];
        for (int i = 1; i <= 3; i++) {
            [flapTextures addObject:
             [SKTexture textureWithImageNamed:
              [NSString stringWithFormat:@"ptero-flap-%d", i]]];
        }
        [SKTexture preloadTextures:flapTextures withCompletionHandler:^{
            dispatch_semaphore_signal(textureSemaphore);
        }];
        SKAction *flap1 = [SKAction animateWithTextures:flapTextures timePerFrame:0.1];
        sharedFlappingAction = [SKAction repeatActionForever:
                                [SKAction sequence:@[flap1, [flap1 reversedAction]]]];

        // Create trailing sparks
        sharedSparkEffect = [NSKeyedUnarchiver unarchiveObjectWithFile:
                             [[NSBundle mainBundle]
                              pathForResource:@"PteroFlying"
                              ofType:@"sks"]];
        sharedSparkEffect.name = @"sparkEffect";
        dispatch_semaphore_wait(textureSemaphore, DISPATCH_TIME_FOREVER);
    });
}

#pragma mark - Performing actions

- (void)startFlapping
{
    if (![self actionForKey:@"flappingAction"]) {
        [self runAction:[sharedFlappingAction copy] withKey:@"flappingAction"];
    }
    if (![self childNodeWithName:@"sparkEffect"]) {
        SKEmitterNode *sparkEffect = [sharedSparkEffect copy];
        sparkEffect.position = CGPointMake(-self.size.width / 2, -self.size.height / 4);
        [self addChild:sparkEffect];
    }
}

- (void)stopFlapping
{
    [self removeActionForKey:@"flappingAction"];
    [[self childNodeWithName:@"sparkEffect"] removeFromParent];
}

- (void)abortActions
{
    _swooping = _leaving = NO;
    [self removeAllActions];
    [self startFlapping];
}

- (BOOL)swoopTo:(CGPoint)location
   withDuration:(NSTimeInterval)swoopDuration
     completion:(void (^)(void))complete
{
    if (_swooping || _leaving) {
        return NO;
    }
    _swooping = YES;
    location.x += self.size.width / 4;
    location.y += self.size.height / 4 ;
    [self runAction:[SKAction moveTo:location duration:swoopDuration]
         completion:^{
             _swooping = NO;
             complete();
         }];
    return YES;
}

- (BOOL)flyAway
{
    if (_swooping || _leaving) {
        return NO;
    }
    CGSize sceneSize = self.scene.size;
    [self runAction:[SKAction sequence:
                     @[[SKAction moveTo:CGPointMake(sceneSize.width + self.size.width,
                                                    sceneSize.height + self.size.height)
                               duration:0.5],
                       [SKAction runBlock:^{ self.hidden = YES; }]
                       ]]
         completion:^{
             _leaving = NO;
         }];
    return YES;
}

@end
