//
//  ViewController.m
//  CorgidactylAnniversary
//
//  Created by James Cash on 02-07-13.
//  Copyright (c) 2013 OccasionallyCogent. All rights reserved.
//

#import "ViewController.h"
#import "CorgoGameScene.h"

@interface ViewController ()
{
    SKScene *_gameScene;
    SKView *_view;
}
@end

@implementation ViewController

- (IBAction)startGame:(id)sender
{
    self.startPlayingButton.hidden = YES;
    self.loadingScreenImage.hidden = YES;

    // Present the scene.
    [_view presentScene:_gameScene];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        NSString *loadingScreenName;
        if ([[UIScreen mainScreen] bounds].size.height == 568) {
            loadingScreenName = @"Default-568h.png";
        } else {
            loadingScreenName = @"Default.png";
        }
        self.loadingScreenImage.image = [UIImage imageNamed:loadingScreenName];
        self.loadingScreenImage.transform = CGAffineTransformMakeRotation(-1.0 * M_PI_2);
    } else {
        self.loadingScreenImage.image = [UIImage imageNamed:@"Default-Landscape.png"];
    }

    [self.loadingIndicator startAnimating];

    [CorgoGameScene loadGameAssetsWithCompletion:^{
        // Configure the view.
        _view = (SKView *)self.view;
        _view.showsFPS = NO;
        _view.showsNodeCount = NO;

        // Create and configure the scene.
        _gameScene = [CorgoGameScene sceneWithSize:CGSizeMake(1024, 768)];
        _gameScene.scaleMode = SKSceneScaleModeAspectFit;

        [self.loadingIndicator stopAnimating];
        self.loadingIndicator.hidden = YES;
        self.startPlayingButton.hidden = NO;
    }];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
