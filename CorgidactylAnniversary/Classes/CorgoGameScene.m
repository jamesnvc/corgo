//
//  MyScene.m
//  CorgidactylAnniversary
//
//  Created by James Cash on 02-07-13.
//  Copyright (c) 2013 OccasionallyCogent. All rights reserved.
//

#import "CorgoGameScene.h"
#import "CorgiSpriteNode.h"
#import "PteroSpriteNode.h"

// Order the game layers should be stacked in, lower numbers appearing closer to the screen
NS_ENUM(NSInteger, GameLayerZOrder) {
    kBackgroundZ,
    kCharacterZ,
    kHUDZ
};

// Names that are used to identify certain SKNodes in the game
static NSString * const kObstacleNodeName = @"obstacle";
static NSString * const kGroundNodeName = @"ground";
static NSString * const kRestartBtnName = @"restart_prompt";
static NSString * const kPteroName = @"ptero";
static NSString * const kHudMsgLabel = @"hud_message";
static NSString * const kPlayPauseName = @"play_pause";

// Fonts used for displaying general information as well as score
static NSString * const kInterfaceFontName = @"Futura-CondensedExtraBold";
// The score updates very rapidly, so use a monospaced font to prevent it jumping around too much
static NSString * const kScoreFontName = @"CourierNew";

// Struct representing the current internal state of the game
typedef struct {
    CGFloat speed; // Speed factor at which the obstacles & background move at
    NSInteger movementRate; // Base rate at which obstacles & boardwalk move
    NSUInteger score; // Current accumlated score
    NSUInteger livesLeft; // Number of lives user has remaining
} GameState;
// Default initial game state
static const GameState INITIAL_STATE = {
    .speed = 1.0,
    .movementRate = 8,
    .score = 0,
    .livesLeft = 3
};

@interface CorgoGameScene () <SKPhysicsContactDelegate>
{
    GameState _gameState;
    CGFloat _groundTop;
    NSArray *_bgSprites;
    NSArray *_layerProcessionRates;
}

@property (nonatomic) SKNode *world;
@property (nonatomic) SKNode *background;
@property (nonatomic) SKNode *foreground;
@property (nonatomic) SKNode *hud;
@property (nonatomic) CorgiSpriteNode *corgi;
@property (nonatomic) PteroSpriteNode *ptero;
@property (nonatomic) SKLabelNode *scoreLabel;
@property (nonatomic,getter=isGameOver) BOOL gameOver;

@end

@implementation CorgoGameScene

#pragma mark - Lifecycle

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        self.physicsWorld.contactDelegate = self;
        self.physicsWorld.gravity = CGVectorMake(0, -5);

        _gameState = INITIAL_STATE;

        // Set the rates at which the various backround layers move relative to each other to establish a parallax effect
        _layerProcessionRates = @[ @0.1, @0.2, @0.3, @0.6, @1];

        // Root node
        _world = [[SKNode alloc] init];
        _world.name = @"world";
        [self addChild:_world];

        // Background layer
        _background = [[SKNode alloc] init];
        _background.name = @"background";
        _background.zPosition = kBackgroundZ;
        [_world addChild:_background];
        [self displayBackgroundLayers];

        // Create the "ground" that the corgi runs along
        SKNode *ground = [SKNode node];
        ground.name = kGroundNodeName;
        ground.position = CGPointMake(0,
                                      CGRectGetMidY(self.frame) - self.size.height / 4);
        ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:
                              CGSizeMake(self.size.width + 20, 50)];
        ground.physicsBody.dynamic = NO;
        ground.physicsBody.categoryBitMask = kCategoryGround;
        ground.physicsBody.collisionBitMask = 0;
        ground.physicsBody.contactTestBitMask = kCategoryCorgi;
        ground.physicsBody.usesPreciseCollisionDetection = YES;
        [_background addChild:ground];
        _groundTop = ground.frame.origin.y;

        // Foreground layer
        _foreground = [[SKNode alloc] init];
        _foreground.zPosition = kCharacterZ;
        [_world addChild:_foreground];

        // Create the corgi & put it in the foreground
        _corgi = [[CorgiSpriteNode alloc] init];
        _corgi.name = @"corgi";
        _corgi.position = CGPointMake(CGRectGetMidX(self.frame) - _corgi.size.width / 2,
                                      _groundTop + _corgi.size.height);
        [_foreground addChild:_corgi];

        // Create the pterodactyl, but keep it hidden until needed
        _ptero = [[PteroSpriteNode alloc] init];
        _ptero.hidden = YES;
        [_foreground addChild:_ptero];

        // HUD layer
        _hud = [[SKNode alloc] init];
        _hud.zPosition = kHUDZ;
        [_world addChild:_hud];

        // Score counter
        _scoreLabel = [SKLabelNode labelNodeWithFontNamed:kScoreFontName];
        _scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeRight;
        _scoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeBottom;
        _scoreLabel.text = @"0pts";
        _scoreLabel.fontSize = 30;
        _scoreLabel.position = CGPointMake(CGRectGetMaxX(self.frame),
                                           CGRectGetMaxY(self.frame) - 30);
        _scoreLabel.zPosition = kHUDZ;
        [_hud addChild:_scoreLabel];

        // Play-pause button
        SKSpriteNode *playPause = [SKSpriteNode spriteNodeWithImageNamed:@"pause"];
        playPause.name = kPlayPauseName;
        playPause.position = CGPointMake(CGRectGetMinX(self.frame) + playPause.size.width,
                                         CGRectGetMaxY(self.frame) - playPause.size.height);
        [_hud addChild:playPause];

        // Display life counter
        [self showIndicatorsForLives:_gameState.livesLeft];

    }
    return self;
}

-(void)update:(CFTimeInterval)currentTime {
    static CFTimeInterval lastObstacleAt = 1;
    static CFTimeInterval pausedLoopAt;
    if (self.paused) {
        if (!pausedLoopAt) {
            pausedLoopAt = currentTime;
        }
        return;
    } else if (pausedLoopAt) {
        lastObstacleAt += currentTime - pausedLoopAt;
        pausedLoopAt = 0;
    }

    // Corgi sometimes gets stuck hovering...
    if (self.corgi.isJumping && self.corgi.physicsBody.isResting) {
        // ...so we nudge it a bit so the physics simulator comes back to life.
        [self.corgi.physicsBody applyImpulse:CGVectorMake(0, -1)];
    }

    // Update score
    _gameState.score += roundf(0.5 * _gameState.speed);
    self.scoreLabel.text = [NSString stringWithFormat:@"%dpts", _gameState.score];

    // Base movement rate for background & obstacles
    float xMovement = _gameState.movementRate * _gameState.speed;

    // Move background
    [_bgSprites enumerateObjectsUsingBlock:^(SKSpriteNode *bgSprite, NSUInteger idx, BOOL *stop) {
        // There are two copies of each background layer, offset by 0.5 their width to allow for continuous scrolling
        NSUInteger layerIdx = floor(idx / 2.0); // This converts array index to logical layer index
        CGPoint bgPos = bgSprite.position;
        // The background layers move at different rates to give a parallax effect
        bgPos.x -= xMovement * [_layerProcessionRates[layerIdx] floatValue];
        if (bgPos.x + bgSprite.size.width / 2.0 <= 0) {
            // If the layer has moved off to the left, put it back on the right side so we can keep scrolling
            bgPos.x += bgSprite.size.width * 2;
        }
        bgSprite.position = bgPos;
    }];

    // Move extant obstacles
    [self.foreground enumerateChildNodesWithName:kObstacleNodeName usingBlock:^(SKNode *obs, BOOL *stop) {
        obs.position = CGPointMake(obs.position.x - xMovement, obs.position.y);
        if (obs.position.x < 0) {
            [obs removeFromParent];
        }
    }];

    // Spawn new obstacles
    if (currentTime > lastObstacleAt + ((2 + arc4random() % 5) / _gameState.speed)) {
        SKSpriteNode *obstacle = [sharedObstacle copy];
        obstacle.position = CGPointMake(CGRectGetMaxX(self.frame) - obstacle.size.width / 2,
                                        _groundTop + obstacle.size.height);
        [self.foreground addChild:obstacle];
        lastObstacleAt = currentTime;
    }
}

#pragma mark - Manage game state

static SKEmitterNode *sharedHeartEffect = nil;
- (void)displayGameOver
{
    [self removeCurrentMessage];
    self.gameOver = YES;
    _gameState.speed = 0;

    [self.background enumerateChildNodesWithName:kObstacleNodeName usingBlock:^(SKNode *node, BOOL *stop) {
        [node removeFromParent];
    }];

    [[self.hud childNodeWithName:kPlayPauseName] setHidden:YES];

    SKLabelNode *scoreDisplay = [SKLabelNode labelNodeWithFontNamed:kInterfaceFontName];
    scoreDisplay.name = @"score-display";
    scoreDisplay.text = [NSString stringWithFormat:@"You earned %d points!", _gameState.score];
    scoreDisplay.fontSize = 50;
    scoreDisplay.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    [self.hud addChild:scoreDisplay];

    if (_gameState.score > [[NSUserDefaults standardUserDefaults] integerForKey:@"highscore"]) {
        [[NSUserDefaults standardUserDefaults] setInteger:_gameState.score forKey:@"highscore"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        SKLabelNode *newHighScore = [SKLabelNode labelNodeWithFontNamed:kInterfaceFontName];
        newHighScore.name = @"new-high-score";
        newHighScore.fontSize = 65;
        newHighScore.text = @"New High Score!";
        newHighScore.position = CGPointMake(CGRectGetMidX(self.frame),
                                            CGRectGetMaxY(self.frame) - newHighScore.frame.size.height * 1.5);
        [newHighScore addChild:[sharedMessageHighlight copy]];
        [self.hud addChild:newHighScore];
        SKAction *wiggle = [SKAction rotateByAngle:M_PI_4 duration:0.5];
        [newHighScore runAction:[SKAction repeatActionForever:
                                 [SKAction sequence:
                                  @[wiggle, [wiggle reversedAction], [wiggle reversedAction], wiggle]]]];
    }

    // Corgi & ptero come together, display hearts particle effect
    CGPoint corgiPteroIntercept = CGPointMake(self.corgi.position.x + self.size.width / 4 + self.corgi.size.width / 2,
                                              self.corgi.position.y + self.corgi.size.height / 4);
    // Corgi runs forwards
    [self.corgi runAction:
     [SKAction sequence:
      @[[SKAction moveByX:self.size.width / 4
                        y:0
                 duration:1],
        [SKAction runBlock:^{
         [self.corgi removeActionForKey:@"runningAction"];
         [[self.corgi childNodeWithName:@"sparkName"] removeFromParent];
     }]]] completion:^{
         // Only display the restart button after the corgi has finished moving fowards to prevent annoying edge cases in cleanup code
         SKLabelNode *restartPrompt = [SKLabelNode labelNodeWithFontNamed:kInterfaceFontName];
         restartPrompt.name = kRestartBtnName;
         restartPrompt.fontSize = 40;
         restartPrompt.text = @"Play Again!";
         restartPrompt.position = CGPointMake(scoreDisplay.position.x, scoreDisplay.position.y - scoreDisplay.frame.size.height);
         [self.hud addChild:restartPrompt];
     }];

    // Ptero swoops in from the left this time
    [self.ptero abortActions];
    self.ptero.xScale = -1;
    self.ptero.position = CGPointMake(CGRectGetMaxX(self.frame) + self.ptero.size.width,
                                      CGRectGetMaxY(self.frame));
    [self.ptero startFlapping];
    self.ptero.physicsBody.dynamic = NO;
    self.ptero.hidden = NO;
    [self.ptero swoopTo:corgiPteroIntercept withDuration:1 completion:^{
        [self.ptero stopFlapping];
        SKEmitterNode *hearts = [sharedHeartEffect copy];
        hearts.position = CGPointMake(self.ptero.size.width / 3, -self.ptero.size.height / 3);
        [self.ptero addChild:hearts];
    }];

}

- (void)restartGame
{
    self.gameOver = NO;

    [[self.hud childNodeWithName:kPlayPauseName] setHidden:NO];

    // Reset ptero
    [self.ptero removeAllChildren];
    [self.ptero startFlapping];
    self.ptero.xScale = 1;
    self.ptero.hidden = YES;
    self.ptero.physicsBody.dynamic = YES;

    // Reset HUD
    for (NSString *nodeName in @[kRestartBtnName, @"score-display", @"new-high-score"]) {
        [[self.hud childNodeWithName:nodeName] removeFromParent];
    }
    _gameState = INITIAL_STATE;
    self.scoreLabel.text = @"0pts";
    [self showIndicatorsForLives:_gameState.livesLeft];

    // Reset corgi
    [self.corgi removeAllActions];
    self.corgi.position = CGPointMake(CGRectGetMidX(self.frame) - _corgi.size.width / 2,
                                      _groundTop + _corgi.size.height);
    [self.corgi.physicsBody applyImpulse:CGVectorMake(0, -1)];
    self.corgi.jumping = YES; // Corgi starts jumping, since she's slightly off the ground to start
    [self.corgi startRunning];
}

#pragma mark - Loading resources

+ (void)loadGameAssets
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Background textures
        dispatch_semaphore_t textureSemphore = dispatch_semaphore_create(0);
        NSMutableArray *bgTextures1 = [NSMutableArray array];
        for (int i = 5; i >= 1; i--) {
            [bgTextures1 addObject:
             [SKTexture textureWithImageNamed:
              [NSString stringWithFormat:@"background-1-%d", i]]];
        }
        sharedBackgroundTextures = [bgTextures1 copy];
        [SKTexture preloadTextures:sharedBackgroundTextures withCompletionHandler:^{
            dispatch_semaphore_signal(textureSemphore);
        }];

        // Life indicator
        sharedLifeIndicator = [SKSpriteNode spriteNodeWithImageNamed:@"lives"];

        // Obstacles
        sharedObstacle = [SKSpriteNode spriteNodeWithImageNamed:@"obstacle"];
        sharedObstacle.name = kObstacleNodeName;
        sharedObstacle.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:sharedObstacle.size];
        sharedObstacle.physicsBody.dynamic = YES;
        sharedObstacle.physicsBody.affectedByGravity = NO;
        sharedObstacle.physicsBody.categoryBitMask = kCategoryObstacle;
        sharedObstacle.physicsBody.collisionBitMask = kCategoryCorgi;
        sharedObstacle.physicsBody.mass = 0.01;

        // Obstacle exploding effect
        explodeEffect = [NSKeyedUnarchiver unarchiveObjectWithFile:
                         [[NSBundle mainBundle]
                          pathForResource:@"Collision"
                          ofType:@"sks"]];

        // Heart shower effect
        sharedHeartEffect = [NSKeyedUnarchiver unarchiveObjectWithFile:
                             [[NSBundle mainBundle]
                              pathForResource:@"CorgiPteroKiss" ofType:@"sks"]];

        // Message box highlight
        sharedMessageHighlight = [NSKeyedUnarchiver unarchiveObjectWithFile:
                                  [[NSBundle mainBundle]
                                   pathForResource:@"MessageBox" ofType:@"sks"]];

        // Random praise phrases for corgi jumping
        superlatives = @[@"Wow", @"Many Meters", @"Such Jump", @"Corgo!"];

        [PteroSpriteNode loadGameAssets];
        [CorgiSpriteNode loadGameAssets];
        dispatch_semaphore_wait(textureSemphore, DISPATCH_TIME_FOREVER);
    });
}

+ (void)loadGameAssetsWithCompletion:(void (^)(void))complete
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self loadGameAssets];
        if (complete) {
            dispatch_async(dispatch_get_main_queue(), complete);
        }
    });
}

static NSArray* sharedBackgroundTextures = nil;
- (void)displayBackgroundLayers
{
    NSMutableArray *bgSprites = [NSMutableArray array];
    [sharedBackgroundTextures enumerateObjectsUsingBlock:^(SKTexture *bgText, NSUInteger idx, BOOL *stop) {
        SKSpriteNode *bgSprite = [SKSpriteNode spriteNodeWithTexture:bgText];
        bgSprite.position = CGPointMake(CGRectGetMidX(self.frame) - 10, CGRectGetMidY(self.frame));

        SKSpriteNode *bgSprite2 = [bgSprite copy];
        CGPoint sprite2Pos = bgSprite.position;
        sprite2Pos.x += bgSprite.size.width;
        bgSprite2.position = sprite2Pos;

        [self.background addChild:bgSprite];
        [self.background addChild:bgSprite2];
        [bgSprites addObject:bgSprite];
        [bgSprites addObject:bgSprite2];
    }];
    _bgSprites = [bgSprites copy];
}

#pragma mark - Interaction

- (void)togglePause
{
    self.paused = !self.paused;
    SKSpriteNode *playPause = (SKSpriteNode *)[self.hud childNodeWithName:kPlayPauseName];
    [playPause setTexture:[SKTexture textureWithImageNamed:(self.paused ? @"play" : @"pause")]];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.isGameOver) {
        SKNode *restartBtn = [self.hud childNodeWithName:kRestartBtnName];
        [touches enumerateObjectsUsingBlock:^(UITouch *touch, BOOL *stop) {
            if ([restartBtn containsPoint:[touch locationInNode:self.hud]]) {
                [self restartGame];
                *stop = YES;
            }
        }];
        return;
    }

    SKNode *pauseBtn = [self.hud childNodeWithName:kPlayPauseName];
    for (UITouch *touch in touches) {
        if ([pauseBtn containsPoint:[touch locationInNode:self.hud]]) {
            [self togglePause];
            return;
        }
        if (self.corgi.isJumping) {
            if ([self isPteroInScene]) {
                [self flyAwayPtero];
            }
            return;
        }
        // Corgi jumps
        CGFloat jumpHeight = self.corgi.size.height * 1.25;
        NSTimeInterval jumpDuration = 0.4;
        // Ptero grabs
        CGPoint intercept = self.corgi.position;
        intercept.y += jumpHeight;

        [self.corgi jumpBy:jumpHeight withDuration:jumpDuration];
        // Pterodactyl appears & gives the corgi a boost with 1/5 probability
        if (![self isPteroInScene] && arc4random() % 5 == 0) {
            [self showPteroBoostMessage];
            [self pteroGrabAt:intercept withDuration:jumpDuration];
        } else {
            [self showJumpPraise];
        }
    }
}

#pragma mark - In-game HUD

static SKSpriteNode *sharedLifeIndicator = nil;
- (void)showIndicatorForLife:(NSUInteger)lifeNumber
{
    SKSpriteNode *lifeNode = [sharedLifeIndicator copy];
    lifeNode.name = [NSString stringWithFormat:@"life-%d", lifeNumber];
    lifeNode.position = CGPointMake(CGRectGetMaxX(self.frame) - lifeNumber * lifeNode.size.width,
                                    CGRectGetMaxY(self.frame) - 60);
    [self.hud addChild:lifeNode];
}

- (void)showIndicatorsForLives:(NSUInteger)totalLives
{
    for (int lifeNumber = 1; lifeNumber <= totalLives; lifeNumber++) {
        [self showIndicatorForLife:lifeNumber];
    }
}

- (void)showPteroBoostMessage
{
    [self showMessage:@"Ptero Boost!" withColor:[UIColor greenColor] andDuration:2];
}

static NSArray *superlatives = nil;
- (void)showJumpPraise
{
    [self showMessage:superlatives[arc4random() % [superlatives count]]
            withColor:[UIColor blueColor]
          andDuration:1];
}

- (void)removeCurrentMessage
{
    [[self.hud childNodeWithName:kHudMsgLabel] removeFromParent];
}

static SKEmitterNode *sharedMessageHighlight = nil;
- (void)showMessage:(NSString *)message withColor:(UIColor *)color andDuration:(NSTimeInterval)duration
{
    [self removeCurrentMessage];
    NSTimeInterval labelInDur = duration / 4;
    NSTimeInterval labelDisplayDur = duration / 2;
    NSTimeInterval labelOutDur = labelInDur;
    SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:kInterfaceFontName];
    label.name = kHudMsgLabel;
    label.fontColor = color;
    label.fontSize = 40;
    label.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    label.text = message;
    [label addChild:[sharedMessageHighlight copy]];
    [self.hud addChild:label];
    [label runAction:[SKAction sequence:
                      @[[SKAction group:@[[SKAction scaleBy:2 duration:labelInDur],
                                          [SKAction rotateByAngle:-M_PI * 0.3 duration:labelInDur]]],
                        [SKAction rotateByAngle:M_PI * 0.3 duration:labelDisplayDur],
                        [SKAction group:@[[SKAction scaleBy:4 duration:labelOutDur],
                                          [SKAction rotateByAngle:-M_PI duration:labelOutDur],
                                          [SKAction fadeAlphaBy:0.5 duration:labelOutDur]]],
                        [SKAction removeFromParent]]]];
}

#pragma mark - Obstacles

static SKSpriteNode *sharedObstacle = nil;

static SKEmitterNode *explodeEffect = nil;
- (void)explodeObstacle:(SKSpriteNode *)obstacle
{
    NSTimeInterval explodeDur = 0.5;
    obstacle.physicsBody = nil;
    [obstacle addChild:[explodeEffect copy]];
    [obstacle runAction:
     [SKAction sequence:@[
                          [SKAction group:
                           @[[SKAction scaleBy:5 duration:explodeDur],
                             [SKAction fadeAlphaTo:0 duration:explodeDur]]],
                          [SKAction removeFromParent]
                          ]]];
}


#pragma mark - Pterodactyl manipulation

- (void)pteroGrabAt:(CGPoint)intercept
       withDuration:(NSTimeInterval)jumpDuration {

    self.ptero.position = CGPointMake(0, CGRectGetMaxY(self.frame));
    self.ptero.hidden = NO;

    [self.ptero swoopTo:intercept withDuration:jumpDuration completion:^{
        CGPoint grabAt = intercept;
        grabAt.y += self.corgi.size.height;
        SKPhysicsJointSliding *joint = [SKPhysicsJointSliding
                                        jointWithBodyA:self.ptero.physicsBody
                                        bodyB:self.corgi.physicsBody
                                        anchor:grabAt
                                        axis:CGVectorMake(0, 1)];
        joint.shouldEnableLimits = YES;
        joint.upperDistanceLimit = 40;
        [self.physicsWorld addJoint:joint];
        _gameState.speed = 3;
    }];
}

- (void)flyAwayPtero
{
    if ([self.ptero flyAway]) {
        _gameState.speed = 1;
        [self.physicsWorld removeAllJoints];
    }
}

- (BOOL)isPteroInScene
{
    return !self.ptero.hidden;
}

#pragma mark - Corgi manipulation

- (void)landCorgi
{
    if (self.corgi.isJumping) {
        [self.corgi landed];
        if ([self isPteroInScene]) {
            [self flyAwayPtero];
        }
    }
}

- (void)corgiHitObstacle:(SKNode *)obstacle
{
    _gameState.score = _gameState.score >= 100 ? _gameState.score - 100 : 0;
    CGFloat explodeLifeDur = 0.5;
    [[self.hud childNodeWithName:[NSString stringWithFormat:@"life-%d",
                                  _gameState.livesLeft]]
     runAction:
     [SKAction sequence:@[
                          [SKAction group:
                           @[[SKAction scaleBy:5 duration:explodeLifeDur],
                             [SKAction fadeAlphaTo:0 duration:explodeLifeDur]]],
                          [SKAction removeFromParent]
                          ]]];
    _gameState.livesLeft -= 1;
    [self.corgi gotHit];
    [self explodeObstacle:(SKSpriteNode *)obstacle];
    if (_gameState.livesLeft == 0) {
        [self landCorgi];
        [self displayGameOver];
    }
}

#pragma mark - SKPhysicsContantDelegate

- (void)didBeginContact:(SKPhysicsContact *)contact
{
    if (self.gameOver) {
        return;
    }
    SKNode *other;
    if ([contact.bodyA.node.name isEqualToString:@"corgi"]) {
        other = contact.bodyB.node;
    } else if ([contact.bodyB.node.name isEqualToString:@"corgi"]) {
        other = contact.bodyA.node;
    } else {
        return;
    }

    switch (other.physicsBody.categoryBitMask) {
        case kCategoryObstacle:
            [self corgiHitObstacle:other];
            break;
        case kCategoryGround:
            [self landCorgi];
            break;
        default:
            break;
    }

}

@end
