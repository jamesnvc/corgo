//
//  CorgiSprite.m
//  CorgidactylAnniversary
//
//  Created by James Cash on 05-07-13.
//  Copyright (c) 2013 OccasionallyCogent. All rights reserved.
//

#import "CorgiSpriteNode.h"
#import "CorgoGameScene.h"

@implementation CorgiSpriteNode

@synthesize jumping = _jumping;

#pragma mark - Lifecycle

- (instancetype)init
{
    if (self = [super initWithImageNamed:@"corgi-base"]) {
        SKEmitterNode *sparkEffect = [sharedSparkEffect copy];
        sparkEffect.position = CGPointMake(-self.size.width / 2, -self.size.height / 4);
        [self addChild:sparkEffect];

        // Create physics body
        self.physicsBody = [SKPhysicsBody
                            bodyWithRectangleOfSize:CGRectInset(self.frame, 30, 10).size];
        self.physicsBody.categoryBitMask = kCategoryCorgi;
        self.physicsBody.collisionBitMask = kCategoryObstacle | kCategoryGround;
        self.physicsBody.contactTestBitMask = kCategoryObstacle | kCategoryGround;
        self.physicsBody.mass = 10;
        self.physicsBody.allowsRotation = NO;

        [self startRunning];
    }
    return self;
}

#pragma mark - Loading resources

static SKAction *sharedRunningAction = nil;
static SKEmitterNode *sharedSparkEffect = nil;
static NSArray *sharedJumpFrames = nil;
+ (void)loadGameAssets
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dispatch_semaphore_t textureSemaphore = dispatch_semaphore_create(0);
        // Create running animation
        NSMutableArray *runTextures = [NSMutableArray array];
        for (int i = 2; i <= 5; i++) {
            [runTextures addObject:
             [SKTexture textureWithImageNamed:
              [NSString stringWithFormat:@"corgi-run-%d", i]]];
        }
        [SKTexture preloadTextures:runTextures withCompletionHandler:^{
            dispatch_semaphore_signal(textureSemaphore);
        }];
        sharedRunningAction = [SKAction repeatActionForever:
                               [SKAction animateWithTextures:runTextures
                                                timePerFrame:0.1]];
        // Create trailing sparks
        sharedSparkEffect = [NSKeyedUnarchiver unarchiveObjectWithFile:
                             [[NSBundle mainBundle]
                              pathForResource:@"CorgiRunning"
                              ofType:@"sks"]];
        sharedSparkEffect.name = @"sparkName";
        // Load jumping sprite frames
        NSMutableArray *jumpTextures = [NSMutableArray array];
        for (int i = 1; i <= 5; i++) {
            [jumpTextures addObject:
             [SKTexture textureWithImageNamed:
              [NSString stringWithFormat:@"corgi-jump-up-%d", i]]];
        }
        [SKTexture preloadTextures:jumpTextures withCompletionHandler:^{
            dispatch_semaphore_signal(textureSemaphore);
        }];
        sharedJumpFrames = [jumpTextures copy];
        dispatch_semaphore_wait(textureSemaphore, DISPATCH_TIME_FOREVER);
        dispatch_semaphore_wait(textureSemaphore, DISPATCH_TIME_FOREVER);
    });
}

#pragma mark - Performing actions

- (SKAction *)jumpActionWithDuration:(CGFloat)duration
{
    return [SKAction animateWithTextures:sharedJumpFrames
                            timePerFrame:duration / ([sharedJumpFrames count])];
}

- (void)startRunning
{
    [self runAction:[sharedRunningAction copy] withKey:@"runningAction"];
}

- (void)jumpBy:(float)jumpHeight withDuration:(NSTimeInterval)jumpDuration
{
    [self removeActionForKey:@"runningAction"];
    _jumping = YES;
    [self runAction:[SKAction group:@[[self jumpActionWithDuration:jumpDuration],
                                      [SKAction moveByX:0 y:jumpHeight duration:jumpDuration],
                                      ]]];
}

- (void)landed
{
    _jumping = NO;
    [self startRunning];
}

- (void)gotHit
{
    // TODO: Don't let this multiple times simultaneously?
    float flashDur = 0.2;
    SKAction *flash1 = [SKAction group:@[[SKAction fadeAlphaBy:0.4 duration:flashDur],
                                         [SKAction colorizeWithColor:[UIColor redColor]
                                                    colorBlendFactor:0.8
                                                            duration:flashDur]]];
    // Ideally this would be [flash1 reverse], but the colourize isn't reversible
    SKAction *flash2 = [SKAction group:@[[SKAction fadeAlphaBy:-0.4 duration:flashDur],
                                         [SKAction colorizeWithColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0]
                                                    colorBlendFactor:0
                                                            duration:flashDur]]];
    SKAction *flash = [SKAction sequence:@[flash1, flash2]];
    [self runAction:[SKAction sequence:@[flash, flash, flash]]];
}

@end
