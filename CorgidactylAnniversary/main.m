//
//  main.m
//  CorgidactylAnniversary
//
//  Created by James Cash on 02-07-13.
//  Copyright (c) 2013 OccasionallyCogent. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
